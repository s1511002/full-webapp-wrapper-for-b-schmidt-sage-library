Web application wrapper around B.Schmidt's SageMath stability conditions library

Clone with subrepos:
```git clone ... --recursive```
Build and run containers (main frontend served on http://0.0.0.0:3000/):
```docker-compose up --build```
